# About
IconCrash is designed to parse the "IconCache.db" file created by the Windows Operating System.  
While forensically valuable, this file is not stored in an easily readable state. IconCrash seperates paths into individual lines and removes extra characters to make the file easier to read.

# Use
By default, IconCrash will parse the current user's "IconCache.db" and store the results on the desktop. To change this behavior, browse to the desired source/destination paths.

# Options
"Open output when finished" - When parsing is finished, the output file (and discard file if end cleaning is enabled) will be opened in the default text editor.  
"Show line addresses" - The hexadecimal offset (relative to the original file) of each line will be prepended in the output file (and discard file if end cleaning is enabled).  
"Try to clean ends" - IconCrash will attempt to remove extra data from the ends of lines. This is based on known file extension matching, and any data discarded will be saved in the discard file.

# Legal
IconCrash is distributed under a CC BY-SA 4.0 license. See https://creativecommons.org/licenses/by-sa/4.0/ for more details.
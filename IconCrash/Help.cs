﻿using System.Windows.Forms;

namespace IconCrash
{
    public partial class Help : Form
    {
        public Help()
        {
            InitializeComponent();
            helpRichTextBox.Rtf = Properties.Resources.helpString;
            versionLabel.Text = string.Format("Version {0}", ProductVersion);
            iconCrashLabel.Parent = versionLabel.Parent = nameLabel.Parent = bgPictureBox;
        }

        private void HelpRichTextBox_LinkClicked(object sender, LinkClickedEventArgs e) => System.Diagnostics.Process.Start(e.LinkText);

        private void helpRichTextBox_Enter(object sender, System.EventArgs e)
        {
            helpLabel.Focus();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IconCrash
{
    public partial class MainWindow : Form
    {
        const int BUFFER_SIZE = 4096, TERMINATOR_SIZE = 8;

        /// <summary>
        ///     Extensions to be trimmed by cleanEnds
        /// </summary>
        private List<string> knownExts = new List<string>() {
            ".cpl",
            ".dll",
            ".exe",
            ".gif",
            ".ico",
            ".jpg",
            ".lnk",
            ".png",
            ".sln",
            ".tmp",
            ".url",
            ".xml"
        };

        /// <summary>
        ///     Source file path
        /// </summary>
        private string sourcePath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\AppData\\Local\\IconCache.db";

        /// <summary>
        ///     Destination file path
        /// </summary>
        private string destinationPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\IconCrash_Output.txt";

        /// <summary>
        ///     Discard file path
        /// </summary>
        private string discardPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\IconCrash_Discarded.txt";

        /// <summary>
        ///     Should output(s) be opened when finished
        /// </summary>
        private bool openOnFinish = Properties.Settings.Default.openOnFinish;

        /// <summary>
        ///     Should line addresses be prepended to output lines
        /// </summary>
        private bool lineAddresses = Properties.Settings.Default.lineAddresses;

        /// <summary>
        ///     Should line end cleaning be attempted
        /// </summary>
        private bool cleanEnds = Properties.Settings.Default.cleanEnds;

        /// <summary>
        ///     Token source for worker
        /// </summary>
        private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        /// <summary>
        ///     Token for worker
        /// </summary>
        private CancellationToken cancellationToken;

        /// <summary>
        ///     Worker state
        /// </summary>
        private bool running = false;

        /// <summary>
        ///     Valid character whitelist
        /// </summary>
        private List<byte> valid = new List<byte>();

        // Colors for UI
        private System.Drawing.Color startColor = System.Drawing.Color.MediumSpringGreen;
        private System.Drawing.Color stopColor = System.Drawing.Color.LightCoral;

        public MainWindow()
        {
            InitializeComponent();

            // UI Init
            openOnFinishCheckBox.Checked = openOnFinish;
            addressesCheckBox.Checked = lineAddresses;
            cleanEndsCheckBox.Checked = cleanEnds;
            SetSrc(sourcePath);
            SetDest(destinationPath);
            startStopButton.BackColor = startColor;
            if (Properties.Settings.Default.srcHistory != null)
                srcTextBox.AutoCompleteCustomSource = Properties.Settings.Default.srcHistory;
            if (Properties.Settings.Default.destHistory != null)
                destTextBox.AutoCompleteCustomSource = Properties.Settings.Default.destHistory;

            // Worker Init
            cancellationToken = cancellationTokenSource.Token;

            // Valid char list Init
            for (byte b = (byte)'a'; b <= (byte)'z'; b++) valid.Add(b);
            for (byte b = (byte)'A'; b <= (byte)'Z'; b++) valid.Add(b);
            for (byte b = (byte)'0'; b <= (byte)'9'; b++) valid.Add(b);
            valid.AddRange(new List<char>() { '\\', '/', ':', '(', ')', '-', '_', '!', '@', '#', '$', '%', '^', '&', '+', '=', '<', '>', '?', '~', '`', '\'', '"', ' ', '.' }.
                ConvertAll(c => Convert.ToByte(c)));
        }

        /// <summary>
        ///     Browse for source file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        public void BrowseSrc(object sender, EventArgs eventArgs)
        {
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
                SetSrc(openFileDialog.FileName);
        }

        /// <summary>
        ///     Set source file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        public void SetSrc(object sender, EventArgs eventArgs)
        {
            try
            {
                SetSrc(((TextBox)sender).Text);
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, "Path Invalid", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                ((Control)sender).Focus();
            }
        }

        /// <summary>
        ///     Set source file
        /// </summary>
        /// <param name="path">Source file path</param>
        private void SetSrc(string path)
        {
            sourcePath = path;
            srcTextBox.Text = path;
            openFileDialog.InitialDirectory = Directory.GetParent(path).ToString();
            openFileDialog.FileName = Path.GetFileName(path).ToString();
        }

        /// <summary>
        ///     Browse for destination file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        public void BrowseDest(object sender, EventArgs eventArgs)
        {
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
                 SetDest(saveFileDialog.FileName);
        }

        /// <summary>
        ///     Set destination file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        public void SetDest(object sender, EventArgs eventArgs)
        {
            try
            { 
                SetDest(((TextBox)sender).Text);
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, "Path Invalid", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                ((Control)sender).Focus();
            }
        }

        /// <summary>
        ///     Set destination file
        /// </summary>
        /// <param name="path">Destination file path</param>
        private void SetDest(string path)
        {
            destinationPath = path;
            discardPath = Directory.GetParent(path).ToString() + "\\IconCrash_Discarded.txt";
            destTextBox.Text = path;
            saveFileDialog.InitialDirectory = Directory.GetParent(path).ToString();
            saveFileDialog.FileName = Path.GetFileName(path);
        }

        /// <summary>
        ///     Change open on finished
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenOnFinishCheckBox_CheckedChanged(object sender, EventArgs e) => openOnFinish = openOnFinishCheckBox.Checked;

        private void ProgressBar1_Click(object sender, EventArgs e) => MessageBox.Show(this, "WOLOLO!", "WOLOLO!", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        /// <summary>
        ///     Toggle run state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void StartStopButton_Click(object sender, EventArgs e)
        {
            if (!running)
            {
                if (File.Exists(destinationPath))
                    switch (MessageBox.Show(this, String.Format("File '{0}' already exists. Overwrite?", destinationPath), "Confirm Overwrite", MessageBoxButtons.OKCancel, MessageBoxIcon.Question))
                    {
                        case DialogResult.OK:
                            break;
                        case DialogResult.Cancel:
                            return;
                    }

                startStopButton.BackColor = stopColor;
                startStopButton.Text = "Stop";
                Progress<int> progress = new Progress<int>(p => progressBar1.Value = p);
                await Task.Factory.StartNew(() => Worker(progress, cancellationToken));
                startStopButton.BackColor = startColor;
                startStopButton.Text = "Start";

                srcTextBox.AutoCompleteCustomSource.Add(sourcePath);
                destTextBox.AutoCompleteCustomSource.Add(destinationPath);
            }
            else
            {
                cancellationTokenSource.Cancel();
                startStopButton.BackColor = startColor;
                startStopButton.Text = "Start";
            }
        }

        /// <summary>
        ///     Worker process
        /// </summary>
        /// <param name="progress">Target for progress percentage (int/100)</param>
        /// <param name="cancellationToken">Target for cancelation token</param>
        private void Worker(IProgress<int> progress, CancellationToken cancellationToken)
        {
            byte[] preBuffer = new byte[BUFFER_SIZE/2];
            byte[] newLine = new byte[] { (byte)'\r', (byte)'\n' };
            List<byte> buffer = new List<byte>(BUFFER_SIZE);
            FileStream readStream, writeStream, discardStream = null;
            List<byte> currentLine = new List<byte>();
            long streamLength;
            bool prefill = true;
            string lastAddress = "";

            // Initialize streams
            try
            {
                readStream = File.OpenRead(sourcePath);
                writeStream = File.OpenWrite(destinationPath);
                if (cleanEnds)
                    discardStream = File.OpenWrite(discardPath);
            }
            catch (Exception e)
            {
                Invoke((MethodInvoker)delegate
                {
                    MessageBox.Show(this, e.Message, "Could not open file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                });
                return;
            }

            streamLength = readStream.Length;

            string head = String.Format("Reading {0:###,##0} Bytes from {1}\r\n", streamLength, sourcePath);
            writeStream.Write(new List<char>(head.ToCharArray()).ConvertAll(c => Convert.ToByte(c)).ToArray(), 0, head.Length);

            // Skip garbage space
            readStream.Seek(79, SeekOrigin.Begin);

            // Process loop
            while (readStream.Read(preBuffer, 0, BUFFER_SIZE/2) > 0)
            {
                //Check for cancel
                if (cancellationToken.IsCancellationRequested)
                    return;

                // Update progress
                progress.Report((int)Math.Ceiling((((double)readStream.Position / streamLength) * 100)));

                // Roll prebuffer into buffer
                if (!prefill)
                    buffer.RemoveRange(0, BUFFER_SIZE / 2);
                buffer.AddRange(preBuffer);

                if (prefill)
                    if (buffer.Count > BUFFER_SIZE / 2)
                        prefill = false;
                    else
                        continue;

                // Search for line terminators
                for (int i = 0; i < BUFFER_SIZE; i++)
                    if (IsLineTerminator(ref buffer, i))
                    {
                        // Clean line
                        currentLine.RemoveAll(b => !valid.Contains(b));

                        if (cleanEnds)
                        {
                            byte[] d = CleanEnd(ref currentLine);

                            if (d != null)
                            {
                                List<byte> discard = new List<byte>(d);
                                if (lineAddresses)
                                    discard.InsertRange(0, new List<char>(lastAddress.ToCharArray()).ConvertAll(c => Convert.ToByte(c)));
                                discard.AddRange(newLine);
                                discardStream.Write(discard.ToArray(), 0, discard.Count);
                            }
                        }

                        // Add line address
                        if (lineAddresses)
                        {
                            currentLine.InsertRange(0, new List<char>(lastAddress.ToCharArray()).ConvertAll(c => Convert.ToByte(c)));
                            lastAddress = String.Format("0x{0:X8} ", (readStream.Position - BUFFER_SIZE) + i + 1);
                        }
                        currentLine.AddRange(newLine);
                        writeStream.Write(currentLine.ToArray(), 0, currentLine.Count);
                        currentLine = new List<byte>();
                    }
                    else
                        // Insert char
                        currentLine.Add(buffer[i]);
            }

            // Cleanup
            readStream.Dispose();
            writeStream.Dispose();
            if (cleanEnds)
                discardStream.Dispose();

            if (openOnFinish)
            {
                if (cleanEnds)
                    System.Diagnostics.Process.Start(discardPath);
                System.Diagnostics.Process.Start(destinationPath);
            }
        }

        /// <summary>
        ///     Determine if a chunk contains a line terminator
        /// </summary>
        /// <param name="buffer">Reference to buffer</param>
        /// <param name="index">Start index of chunk</param>
        /// <returns>Terminator found</returns>
        private bool IsLineTerminator(ref List<byte> buffer, int index, bool recurse = true)
        {
            if (index + TERMINATOR_SIZE >= buffer.Count)
                return false;

            // Drive Path (e.g. C:\Dir\App)
            bool drivePath = (
                buffer[index + 0] == 0x00 &&
                valid.Contains(buffer[index + 1]) &&
                buffer[index + 2] == 0x00 &&
                buffer[index + 3] == ':' &&
                buffer[index + 4] == 0x00 &&
                buffer[index + 5] == '\\' &&
                buffer[index + 6] == 0x00 &&
                valid.Contains(buffer[index + 7])
                );

            // Env Var (e.g. %systemroot%/App)
            bool envVar = (
                buffer[index + 0] == 0x00 &&
                buffer[index + 1] == '%' &&
                buffer[index + 2] == 0x00 &&
                valid.Contains(buffer[index + 3]) && buffer[index + 3] != '\\' &&
                buffer[index + 4] == 0x00 &&
                valid.Contains(buffer[index + 5]) &&
                buffer[index + 6] == 0x00 &&
                valid.Contains(buffer[index + 7])
                );

            // Windows Store (e.g. @pub.app)
            bool winStore = (
                buffer[index + 0] == 0x00 &&
                buffer[index + 1] == '@' &&
                buffer[index + 2] == 0x00 &&
                valid.Contains(buffer[index + 3]) &&
                buffer[index + 4] == 0x00 &&
                valid.Contains(buffer[index + 5]) &&
                buffer[index + 6] == 0x00 &&
                valid.Contains(buffer[index + 7])
                );

            // Network share
            bool netShare = (
                buffer[index + 0] == 0x00 &&
                buffer[index + 1] == '\\' &&
                buffer[index + 2] == 0x00 &&
                buffer[index + 3] == '\\' &&
                buffer[index + 4] == 0x00 &&
                valid.Contains(buffer[index + 5]) &&
                buffer[index + 6] == 0x00 &&
                valid.Contains(buffer[index + 7])
                );

            if ( drivePath || envVar || winStore || netShare )
                // Check proceeding index for double terminator
                if (recurse ? !IsLineTerminator(ref buffer, index + 2, false) : true)
                    return true;
            return false;
        }

        /// <summary>
        ///     Change if ends are cleaned
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CleanEndsCheckBox_CheckedChanged(object sender, EventArgs e) => cleanEnds = cleanEndsCheckBox.Checked;

        /// <summary>
        ///     Show about dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e) => new Help().ShowDialog();

        /// <summary>
        ///     Save settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.srcHistory = srcTextBox.AutoCompleteCustomSource;
            Properties.Settings.Default.destHistory = destTextBox.AutoCompleteCustomSource;
            Properties.Settings.Default.openOnFinish = openOnFinish;
            Properties.Settings.Default.lineAddresses = lineAddresses;
            Properties.Settings.Default.cleanEnds = cleanEnds;
            Properties.Settings.Default.Save();
        }

        /// <summary>
        ///     Change if line addresses are shown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddressesCheckBox_CheckedChanged(object sender, EventArgs e) => lineAddresses = addressesCheckBox.Checked;

        /// <summary>
        ///     Trim line end to last known extension
        /// </summary>
        /// <param name="line">Target line to trim</param>
        /// <returns>Discarded bytes</returns>
        private byte[] CleanEnd(ref List<byte> line)
        {
            int lastIndex = 0;
            string lastExt = "";
            string sLine = new string(line.ConvertAll(b => Convert.ToChar(b)).ToArray()).ToLower();

            foreach (string ext in knownExts)
                if (sLine.Contains(ext))
                {
                    int i = sLine.LastIndexOf(ext);
                    if (i > lastIndex)
                    {
                        lastIndex = i;
                        lastExt = ext;
                    }
                }

            if (lastIndex > 0)
            {
                byte[] discard = line.GetRange(lastIndex + lastExt.Length, line.Count - (lastIndex + lastExt.Length)).ToArray();
                line.RemoveRange(lastIndex + lastExt.Length, line.Count - (lastIndex + lastExt.Length));
                return discard;
            }
            return null;
        }
    }
}

﻿namespace IconCrash
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.srcTextBox = new System.Windows.Forms.TextBox();
            this.srcBrowseButton = new System.Windows.Forms.Button();
            this.helpButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.destTextBox = new System.Windows.Forms.TextBox();
            this.destBrowseButton = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.startStopButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.openOnFinishCheckBox = new System.Windows.Forms.CheckBox();
            this.addressesCheckBox = new System.Windows.Forms.CheckBox();
            this.cleanEndsCheckBox = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.srcTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.srcBrowseButton, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.helpButton, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.destTextBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.destBrowseButton, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.progressBar1, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.startStopButton, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(584, 117);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IconCache.db";
            // 
            // srcTextBox
            // 
            this.srcTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.srcTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.srcTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.srcTextBox.Location = new System.Drawing.Point(83, 3);
            this.srcTextBox.Name = "srcTextBox";
            this.srcTextBox.Size = new System.Drawing.Size(417, 20);
            this.srcTextBox.TabIndex = 1;
            this.srcTextBox.Leave += new System.EventHandler(this.SetSrc);
            // 
            // srcBrowseButton
            // 
            this.srcBrowseButton.Location = new System.Drawing.Point(506, 3);
            this.srcBrowseButton.Name = "srcBrowseButton";
            this.srcBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.srcBrowseButton.TabIndex = 2;
            this.srcBrowseButton.Text = "Browse";
            this.srcBrowseButton.UseVisualStyleBackColor = true;
            this.srcBrowseButton.Click += new System.EventHandler(this.BrowseSrc);
            // 
            // helpButton
            // 
            this.helpButton.Location = new System.Drawing.Point(506, 61);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(75, 23);
            this.helpButton.TabIndex = 10;
            this.helpButton.Text = "Help (?)";
            this.helpButton.UseVisualStyleBackColor = true;
            this.helpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Destination";
            // 
            // destTextBox
            // 
            this.destTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.destTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.destTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.destTextBox.Location = new System.Drawing.Point(83, 32);
            this.destTextBox.Name = "destTextBox";
            this.destTextBox.Size = new System.Drawing.Size(417, 20);
            this.destTextBox.TabIndex = 4;
            this.destTextBox.Leave += new System.EventHandler(this.SetDest);
            // 
            // destBrowseButton
            // 
            this.destBrowseButton.Location = new System.Drawing.Point(506, 32);
            this.destBrowseButton.Name = "destBrowseButton";
            this.destBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.destBrowseButton.TabIndex = 5;
            this.destBrowseButton.Text = "Browse";
            this.destBrowseButton.UseVisualStyleBackColor = true;
            this.destBrowseButton.Click += new System.EventHandler(this.BrowseDest);
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.progressBar1.Location = new System.Drawing.Point(83, 90);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(417, 23);
            this.progressBar1.TabIndex = 8;
            this.progressBar1.Click += new System.EventHandler(this.ProgressBar1_Click);
            // 
            // startStopButton
            // 
            this.startStopButton.Location = new System.Drawing.Point(506, 90);
            this.startStopButton.Name = "startStopButton";
            this.startStopButton.Size = new System.Drawing.Size(75, 23);
            this.startStopButton.TabIndex = 6;
            this.startStopButton.Text = "Start";
            this.startStopButton.UseVisualStyleBackColor = false;
            this.startStopButton.Click += new System.EventHandler(this.StartStopButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 90);
            this.label3.Margin = new System.Windows.Forms.Padding(3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Progress";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.openOnFinishCheckBox);
            this.flowLayoutPanel1.Controls.Add(this.addressesCheckBox);
            this.flowLayoutPanel1.Controls.Add(this.cleanEndsCheckBox);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(83, 61);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(417, 23);
            this.flowLayoutPanel1.TabIndex = 10;
            // 
            // openOnFinishCheckBox
            // 
            this.openOnFinishCheckBox.AutoSize = true;
            this.openOnFinishCheckBox.Location = new System.Drawing.Point(3, 3);
            this.openOnFinishCheckBox.Name = "openOnFinishCheckBox";
            this.openOnFinishCheckBox.Size = new System.Drawing.Size(153, 17);
            this.openOnFinishCheckBox.TabIndex = 9;
            this.openOnFinishCheckBox.Text = "Open output when finished";
            this.openOnFinishCheckBox.UseVisualStyleBackColor = true;
            this.openOnFinishCheckBox.CheckedChanged += new System.EventHandler(this.OpenOnFinishCheckBox_CheckedChanged);
            // 
            // addressesCheckBox
            // 
            this.addressesCheckBox.AutoSize = true;
            this.addressesCheckBox.Location = new System.Drawing.Point(162, 3);
            this.addressesCheckBox.Name = "addressesCheckBox";
            this.addressesCheckBox.Size = new System.Drawing.Size(123, 17);
            this.addressesCheckBox.TabIndex = 11;
            this.addressesCheckBox.Text = "Show line addresses";
            this.addressesCheckBox.UseVisualStyleBackColor = true;
            this.addressesCheckBox.CheckedChanged += new System.EventHandler(this.AddressesCheckBox_CheckedChanged);
            // 
            // cleanEndsCheckBox
            // 
            this.cleanEndsCheckBox.AutoSize = true;
            this.cleanEndsCheckBox.Location = new System.Drawing.Point(291, 3);
            this.cleanEndsCheckBox.Name = "cleanEndsCheckBox";
            this.cleanEndsCheckBox.Size = new System.Drawing.Size(108, 17);
            this.cleanEndsCheckBox.TabIndex = 12;
            this.cleanEndsCheckBox.Text = "Try to clean ends";
            this.cleanEndsCheckBox.UseVisualStyleBackColor = true;
            this.cleanEndsCheckBox.CheckedChanged += new System.EventHandler(this.CleanEndsCheckBox_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 61);
            this.label4.Margin = new System.Windows.Forms.Padding(3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Misc.";
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "db";
            this.openFileDialog.FileName = "openFileDialog";
            this.openFileDialog.Filter = "IconCache files|*.db|All files|*.*";
            this.openFileDialog.Title = "IconCache.db";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "txt";
            this.saveFileDialog.Filter = "Text files|*.txt|All files|*.*";
            this.saveFileDialog.Title = "Destination";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 117);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "IconCrash";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox srcTextBox;
        private System.Windows.Forms.Button srcBrowseButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox destTextBox;
        private System.Windows.Forms.Button destBrowseButton;
        private System.Windows.Forms.Button startStopButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox openOnFinishCheckBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button helpButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox addressesCheckBox;
        private System.Windows.Forms.CheckBox cleanEndsCheckBox;
    }
}


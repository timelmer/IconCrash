﻿namespace IconCrash
{
    partial class Help
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Help));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.helpLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nameLabel = new System.Windows.Forms.Label();
            this.versionLabel = new System.Windows.Forms.Label();
            this.iconCrashLabel = new System.Windows.Forms.Label();
            this.bgPictureBox = new System.Windows.Forms.PictureBox();
            this.helpRichTextBox = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bgPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.helpLabel, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.helpRichTextBox, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 450);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // helpLabel
            // 
            this.helpLabel.AutoSize = true;
            this.helpLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpLabel.Location = new System.Drawing.Point(403, 3);
            this.helpLabel.Margin = new System.Windows.Forms.Padding(3);
            this.helpLabel.Name = "helpLabel";
            this.helpLabel.Size = new System.Drawing.Size(64, 29);
            this.helpLabel.TabIndex = 0;
            this.helpLabel.Text = "Help";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.nameLabel);
            this.panel1.Controls.Add(this.versionLabel);
            this.panel1.Controls.Add(this.iconCrashLabel);
            this.panel1.Controls.Add(this.bgPictureBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 2);
            this.panel1.Size = new System.Drawing.Size(400, 450);
            this.panel1.TabIndex = 1;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.BackColor = System.Drawing.Color.Transparent;
            this.nameLabel.Location = new System.Drawing.Point(164, 269);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(73, 39);
            this.nameLabel.TabIndex = 2;
            this.nameLabel.Text = "CC BY-SA 4.0\r\n\r\nTimothy Elmer";
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.BackColor = System.Drawing.Color.Transparent;
            this.versionLabel.Location = new System.Drawing.Point(161, 206);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(78, 13);
            this.versionLabel.TabIndex = 1;
            this.versionLabel.Text = "Version 0.0.0.0";
            // 
            // iconCrashLabel
            // 
            this.iconCrashLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.iconCrashLabel.AutoSize = true;
            this.iconCrashLabel.BackColor = System.Drawing.Color.Transparent;
            this.iconCrashLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iconCrashLabel.Location = new System.Drawing.Point(119, 169);
            this.iconCrashLabel.Name = "iconCrashLabel";
            this.iconCrashLabel.Size = new System.Drawing.Size(163, 37);
            this.iconCrashLabel.TabIndex = 0;
            this.iconCrashLabel.Text = "IconCrash";
            this.iconCrashLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bgPictureBox
            // 
            this.bgPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("bgPictureBox.Image")));
            this.bgPictureBox.InitialImage = null;
            this.bgPictureBox.Location = new System.Drawing.Point(3, 3);
            this.bgPictureBox.Name = "bgPictureBox";
            this.bgPictureBox.Size = new System.Drawing.Size(394, 444);
            this.bgPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bgPictureBox.TabIndex = 3;
            this.bgPictureBox.TabStop = false;
            // 
            // helpRichTextBox
            // 
            this.helpRichTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.helpRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.helpRichTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.helpRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.helpRichTextBox.Location = new System.Drawing.Point(403, 38);
            this.helpRichTextBox.Name = "helpRichTextBox";
            this.helpRichTextBox.ReadOnly = true;
            this.helpRichTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.helpRichTextBox.ShortcutsEnabled = false;
            this.helpRichTextBox.Size = new System.Drawing.Size(394, 409);
            this.helpRichTextBox.TabIndex = 2;
            this.helpRichTextBox.Text = "Words go here";
            this.helpRichTextBox.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.HelpRichTextBox_LinkClicked);
            this.helpRichTextBox.Enter += new System.EventHandler(this.helpRichTextBox_Enter);
            // 
            // Help
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Help";
            this.Text = "Help";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bgPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label helpLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.Label iconCrashLabel;
        private System.Windows.Forms.RichTextBox helpRichTextBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.PictureBox bgPictureBox;
    }
}